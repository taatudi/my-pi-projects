#!/usr/bin/env python

''' pastehtml.py

A Python client for the pastehtml.com API
github.com/rafjaa/Python-PasteHTML-API

'''

import urllib
from sys import argv

class PasteHTML:
    URL = 'http://pastehtml.com/upload/create?input_type={0}&result={1}'
    INPUT_VALUES = ('txt', 'html', 'mrk')
    RESULT_VALUES = ('address', 'redirect')

    def post(self, content, input_type='html', result='address'):
        if input_type not in self.INPUT_VALUES:
            input_type = 'html'

        if result not in self.RESULT_VALUES:
            result = 'address'

        param = urllib.urlencode({'txt': content})
        conn = urllib.urlopen(self.URL.format(input_type, result), param)
        return conn.read()


if __name__ == '__main__':
    if len(argv) < 2 or len(argv) > 3:
        print '\nUsage: python pastehtml.py content [html|txt|mrk]\n'
        exit()
    
    content = argv[1]
    input_type = 'html'
    
    if len(argv) == 3:
        input_type = argv[2]
    
    pasteHTML = PasteHTML()
    url = pasteHTML.post(content, input_type=input_type)
    print '\nContent published at {0}\n'.format(url)


