#!/usr/bin/env python
# PiBot, an IRC Bot designed for my RasPi (by Taatudi)

import sys
import socket
import ssl
import time
import re
import urllib

from pastehtml import PasteHTML

NICK='PiBot'
IDENT='PiBot'
REALNAME='PiBot'
CHAN='#bots'
PASSWORD='p1b07'
CMDPREFIX = 'pi.'
data=""
stayonline = True
logbuffer = ''

regex = re.compile('(?P<user>\\S+) (?P<command>\\S+) (?P<channel>\\S+) (?P<message>.+)')

def print_debug(msg):
    global logbuffer
    print '[DEBUG]', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), msg
    #logbuffer += '<p class="debug" style="color:#0F0"><b>'+'[DEBUG] '+time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())+' '+msg+'</b></p>\n'

def print_warning(msg):
    global logbuffer
    print '[!!WARNING!!]', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), msg
    logbuffer += '<p class="warning" style="color:#F00"><b>'+'[!!WARNING!!] '+time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())+' '+msg+'</b></p>\n'

def print_message(channel, user, msg):
    global logbuffer
    print '[MSG '+channel+']', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),user+':', msg
    logbuffer += '<p class="message">'+'[MSG '+channel+'] '+ time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())+' '+user+': '+ msg+'</p>\n'
    
def print_command_request(channel, user, msg):
    global logbuffer
    print '[CMD '+channel+']', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),user+':', msg
    logbuffer += '<p class="cmdreq" style="color:#00F">'+'[CMD '+channel+'] '+ time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())+' '+user+': '+ msg+'</p>\n'

def send_message(channel, msg):
    global logbuffer
    print '[SEND '+channel+']', time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()), msg
    logbuffer += '<p class="send" style="color:#00F">'+'[SEND '+channel+'] '+ time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())+' '+ msg+'</p>\n'
    ircsock.send('PRIVMSG '+channel+' :'+msg+'\r\n')

# -----========[ BOT FUNCTIONS ]========-----

def link_log(channel, user, args):
    '''posts link for log file'''
    print_debug('requesting log file')
    ph = PasteHTML()
    send_message(channel, 'log: '+ph.post(logbuffer))

def quit(channel, user, args):
    '''shuts down PiBot'''
    global stayonline
    #TODO: check user
    if (user == 'FakeMessiah'):
        stayonline = False
        print_debug('shutting down (quit command used by '+user+')')
    else:
        send_message(channel, 'Permission Denied!')

def join_channel(channel, user, args):
    '''joins given channel'''
    #TODO: check user
    if (user == 'FakeMessiah'):
        print_debug('joining '+args+' (join command used by '+user+')')
        ircsock.send('JOIN '+args+'\r\n')
    else:
        send_message(channel, 'Permission Denied!')

def hello_world(channel, user, args):
    '''secret :P'''
    send_message(channel, 'Hello '+user+' :)')
    
def help(channel, user, args):
    '''prints function list'''
    if args != '' and args in commands_list:
        send_message(channel, args + ': ' +commands_list[args].__doc__)
    else:
        send_message(channel, 'Supported functions (pi.help <function_name> for more help):')
        funcs = ''
        for n in commands_list:
            funcs += n + ' '
        send_message(channel, funcs)

commands_list = {'quit':quit,'log':link_log, 'join':join_channel, 'hello':hello_world, 'help':help}
# -----========[   MAIN LOOP   ]=========-----
print_debug('PiBot started')
s=socket.socket ( socket.AF_INET, socket.SOCK_STREAM )
s.connect ( ( 'irc.psych0tik.net', 6697 ) )
ircsock=ssl.wrap_socket( s )
ircsock.send ( 'USER %s %s %s :%s\r\n' %(NICK, IDENT, REALNAME, NICK ) )
ircsock.send ( 'NICK %s\r\n' % NICK )
print_debug('Connected')
while stayonline:
    data = ircsock.recv (4096)
    lines = data.split('\r\n')
    for line in lines:
        line.replace('\r\n', '')
        if (line == ''):
            continue
        if (line[:4] == 'PING'):
            print_debug('got pinged ('+line+')')
            ircsock.send ('PONG' + line[4:] + '\r\n' )
            continue
        line = line[1:]
        cmd = channel = user = message = ''
        res = regex.match(line)
        
        if (res == None):
            print_warning('RegEx failed ('+line+')')
            continue
        
        user = res.group('user')
        if (user.find('!') != -1):
            user = user.split('!')[0]
        
        cmd = res.group('command')
        
        channel = res.group('channel')
        
        message = res.group('message')
        
        if (cmd == '001'):
            print_debug('joining '+CHAN)
            ircsock.send('JOIN '+CHAN+'\r\n')
        
        if (cmd != 'PRIVMSG'):
            print_debug(line)
        else:
            message = message[1:]
            if message.startswith(CMDPREFIX):
                cmdreq = message[len(CMDPREFIX):]
                if cmdreq.find(' ') != -1:
                    cmdreq = cmdreq.split(' ')
                else:
                    cmdreq = [cmdreq, '']
                
                print_command_request(channel, user, cmdreq[0]+ ' '+cmdreq[1])
                if cmdreq[0] in commands_list:
                    commands_list[cmdreq[0]](channel, user, cmdreq[1])
                else:
                    send_message(channel, 'command '+cmdreq[0]+' not found!')
            else:
                print_message(channel, user, message)

logfile = open('LOG.html', 'w')
logfile.write(logbuffer)
logfile.close()